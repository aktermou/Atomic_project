<?php

namespace App\City;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class City extends DB
{
    private $id;
    private $city_name;

    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("city_name",$postData)){
            $this->city_name = $postData["city_name"];
        }
    }

    public function store(){
        $dataArray=array($this->city_name);
        $sql="INSERT INTO city(city_name) VALUES (?)";


        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted <br>");
        }else{
            Message::message("Data has not been inserted <br>");
        }

        Utility::redirect("create.php");
    }

}