<?php

namespace App\BirthDay;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class BirthDay extends DB
{
    private $id;
    private $name;
    private $birthdate;

    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("dateOfBirth",$postData)){
            $this->birthdate = $postData["dateOfBirth"];
        }
    }

    public function store(){
        $dataArray=array($this->name,$this->birthdate);
        $sql="INSERT INTO birth_day(name,birthdate) VALUES (?,?)";


        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted <br>");
        }else{
            Message::message("Data has not been inserted <br>");
        }

        Utility::redirect("create.php");
    }

}