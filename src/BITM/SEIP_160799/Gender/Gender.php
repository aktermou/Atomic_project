<?php

namespace App\Gender;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class Gender extends DB
{
    private $id;
    private $name;
    private $gender;
    private $soft_deleted;

    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("gender",$postData)){
            $this->gender = $postData["gender"];
        }

        if(array_key_exists("softDeleted",$postData)){
            $this->soft_deleted = $postData["softDeleted"];
            echo $this->soft_deleted;
        }

    }

    public function store(){
        $dataArray=array($this->name,$this->gender);
        $sql="INSERT INTO gender(name,gender) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted <br>");
        }else{
            Message::message("Data has not been inserted <br>");
        }

        Utility::redirect("create.php");
    }

}