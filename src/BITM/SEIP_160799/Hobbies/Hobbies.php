<?php

namespace App\Hobbies;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;

class Hobbies extends DB
{
    private $id;
    private $name;
    private $hobbies;
    private $soft_deleted;

    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("hobbies",$postData)){
            $this->hobbies = implode(',',$postData["hobbies"]);
        }

        if(array_key_exists("softDeleted",$postData)){
            $this->soft_deleted = $postData["softDeleted"];
            echo $this->soft_deleted;
        }

    }

    public function store(){
        $dataArray=array($this->name,$this->hobbies);
        $sql="INSERT INTO hobbies(name,hobbies) VALUES (?,?)";
        //var_dump($dataArray);

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted <br>");
        }else{
            Message::message("Data has not been inserted <br>");
        }

        Utility::redirect("create.php");

    }

}