<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg=Message::message();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">

</head>
<body class="container">

<div class="page-header">
    <h1>Hobbies</h1>
</div>

<form class="form-horizontal" action="store.php" method="post">
    <div class="form-group">

        <label class="col-sm-2" style="text-decoration-color: red">Name:</label>
        <input class="form-group" type="text" name="name" placeholder="Enter Your Name"><br>

        <label class="col-sm-2" style="text-decoration-color: red">Hobbies:</label>
        <label>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Gardening " > Gardening
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value=" Reading  " > Reading
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Photography " >Photography
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Painting " >Painting
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Facebooking " >Facebooking
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Cooking " >Cooking
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Programming " >Programming
            </div>
            <div class="checkbox">
                <input class="checkbox" type="checkbox" name="hobbies[]" value="Dancing " >Dancing
            </div>
        </label><p>
        <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" class="btn btn-primary" style="width: 100px;" value="Submit">
        </div></p>
    </div>
    <style>
        form{
            width:330px:
            margin:0 auto;
            padding:50px;
            background-color: pink;
        }

    </style>
</form>


    jQuery(function($){
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>